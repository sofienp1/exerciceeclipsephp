
//une chaine de caractere

<?php
echo "Hello World!";
echo "PHP is so easy!";
?>


//2 variable entier
<?php
function f($arg){
  var_dump($arg);
  return $arg;
}
echo "Foo" . f("bar") . "Foo";
echo "\n\n";
echo "Foo", f("bar"), "Foo";
?>


//une chaine de c contenant une variable
<?php
function f($arg){
  var_dump($arg);
  return $arg;
}
echo "Foo" . f("bar") . "Foo";
echo "\n\n";
echo "Foo", f("bar"), "Foo";
?>

//3 afficher un texte selon condition vrai ou faux avec php

<?php
if(3>2){
    echo "vrai";
}else{
    echo "faux";
}
?>



